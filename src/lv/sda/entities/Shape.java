package lv.sda.entities;

public interface Shape {
    Integer getPerimeter();
    Integer getArea();

    default Colors getBorderColor() {
        return Colors.BLACK;
    }
}
