package lv.sda.entities;

public class Human extends Entity {

    @Override
    String getName() {
        return "I am human";
    }

    @Override
    public int sumOfTwo(int n1, int n2) {
        return 0;
    }

    @Override
    public int sumOfThree(int n1, int n2, int n3) {
        return 0;
    }

}
