package lv.sda.entities;

public class Star implements Shape, Color {

    @Override
    public String getColorName() {
        return null;
    }

    @Override
    public Integer getPerimeter() {
        return null;
    }

    @Override
    public Integer getArea() {
        return null;
    }

    @Override
    public String getBorderColor() {
        return "golden";
    }
}
