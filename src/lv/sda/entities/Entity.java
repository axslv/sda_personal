package lv.sda.entities;

public abstract class Entity {
    private String name;

     String getName() {
        return this.name;
    }

    public abstract int sumOfTwo(int n1, int n2);

    public abstract int sumOfThree(int n1, int n2, int n3);

}
