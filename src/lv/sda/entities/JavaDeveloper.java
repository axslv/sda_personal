package lv.sda.entities;

public class JavaDeveloper extends Developer {
    public JavaDeveloper() {
        super();
        System.out.println("Constructor for JavaDeveloper was called");
    }

    @Override
    public void overloadMe() {

    }

    public void overloadMe(String parameter) {

    }

}
