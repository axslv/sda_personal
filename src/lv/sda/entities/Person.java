package lv.sda.entities;

public class Person {
    String name;
    String email;
    String personCode;
    Integer age;
    Gender gender = Gender.MALE;

    public Person() {
        System.out.println("Constructor for Person was called");
        System.out.println(gender.getDescription());
        Gender.values();
    }

    public void overloadMe() {

    }
}
